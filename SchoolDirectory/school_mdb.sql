CREATE TABLE Workers(
	worker_id GUID NOT NULL,
	worker_position_id GUID NOT NULL,
	workers_data_id GUID,
	last_name CHAR(20),
	first_name CHAR(20),
	middle_name CHAR(20),
	CONSTRAINT Workers PRIMARY KEY (worker_id)
);

CREATE TABLE Workers_data(
	workers_data_id GUID NOT NULL,
	worker_login CHAR(20) NOT NULL,
	worker_password CHAR(20) NOT NULL,
	CONSTRAINT Workers_data_PK PRIMARY KEY (workers_data_id)
);

CREATE TABLE Positions(
	positions_id GUID NOT NULL,
	name_position CHAR(18) NOT NULL,
	status INT NOT NULL,
	CONSTRAINT Positions_PK PRIMARY KEY (positions_id)
);

CREATE TABLE Documents(
	workers_id GUID NOT NULL,
	doc_id GUID NOT NULL,
	acting_workers_id GUID,
	start_date DATETIME NOT NULL,
	end_date DATETIME,
	realization_date DATETIME,
	doc_path TEXT(250),
	doc_type INT,
	CONSTRAINT Documents_PK PRIMARY KEY (doc_id)
);

ALTER TABLE Workers
    ADD CONSTRAINT Workers_FK_Data
        FOREIGN KEY (workers_data_id) REFERENCES Workers_data(workers_data_id) ON UPDATE CASCADE ON DELETE CASCADE;
		
ALTER TABLE Documents
    ADD CONSTRAINT Documents_FK_Workers
        FOREIGN KEY (workers_id) REFERENCES Workers(worker_id) ON UPDATE CASCADE ON DELETE CASCADE;

INSERT INTO Positions (positions_id,name_position,status) VALUES ('186f94e7-1881-4ab3-ae5d-84decd09b8f5','��������', 0);
INSERT INTO Positions (positions_id,name_position,status) VALUES ('b9822695-dfb5-42a9-b763-9eb329084446','�������', 1);

INSERT INTO Workers_data (workers_data_id,worker_login,worker_password) VALUES ('7ef5c867-5a3b-4b83-b11a-4049b0e706ec','Director', '123456');
INSERT INTO Workers_data (workers_data_id,worker_login,worker_password) VALUES ('bf773b29-1565-4d3a-b3fb-a3e46b1da4b9','Teacher', '123456');

INSERT INTO Workers (worker_id,worker_position_id,workers_data_id,last_name,first_name,middle_name) VALUES ('6f8461f1-a869-4b77-afb4-adb1ff422763',
	'186f94e7-1881-4ab3-ae5d-84decd09b8f5', '7ef5c867-5a3b-4b83-b11a-4049b0e706ec','������', '����', '��������');

INSERT INTO Workers (worker_id,worker_position_id,workers_data_id, last_name,first_name,middle_name) VALUES ('fb4d18be-3729-464b-ac7e-e9a6e79a1f86',
	'b9822695-dfb5-42a9-b763-9eb329084446', 'bf773b29-1565-4d3a-b3fb-a3e46b1da4b9','������2', '����2', '��������2');
