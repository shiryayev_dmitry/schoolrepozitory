﻿namespace SchoolDirectory
{
    partial class FCreateDocument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.lType = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.dtpSigningDate = new System.Windows.Forms.DateTimePicker();
            this.lDateSigning = new System.Windows.Forms.Label();
            this.dtpDateOfAppointment = new System.Windows.Forms.DateTimePicker();
            this.lDateOfAppoiuntment = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lDocPath = new System.Windows.Forms.Label();
            this.btnGetRespons = new System.Windows.Forms.Button();
            this.tbMiddleName = new System.Windows.Forms.TextBox();
            this.tbFirstName = new System.Windows.Forms.TextBox();
            this.tbLastName = new System.Windows.Forms.TextBox();
            this.lName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "приказ",
            "распоряжение"});
            this.comboBox1.Location = new System.Drawing.Point(133, 12);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(324, 24);
            this.comboBox1.TabIndex = 29;
            // 
            // lType
            // 
            this.lType.AutoSize = true;
            this.lType.Location = new System.Drawing.Point(20, 15);
            this.lType.Name = "lType";
            this.lType.Size = new System.Drawing.Size(107, 17);
            this.lType.TabIndex = 28;
            this.lType.Text = "Тип документа";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(512, 296);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(92, 26);
            this.button2.TabIndex = 27;
            this.button2.Text = "обзор ";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(610, 296);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(84, 27);
            this.button1.TabIndex = 26;
            this.button1.Text = "сохранить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dtpSigningDate
            // 
            this.dtpSigningDate.CustomFormat = "dd.MM.yyyy H:m:s";
            this.dtpSigningDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSigningDate.Location = new System.Drawing.Point(133, 202);
            this.dtpSigningDate.Name = "dtpSigningDate";
            this.dtpSigningDate.Size = new System.Drawing.Size(171, 22);
            this.dtpSigningDate.TabIndex = 25;
            // 
            // lDateSigning
            // 
            this.lDateSigning.AutoSize = true;
            this.lDateSigning.Location = new System.Drawing.Point(28, 202);
            this.lDateSigning.Name = "lDateSigning";
            this.lDateSigning.Size = new System.Drawing.Size(99, 17);
            this.lDateSigning.TabIndex = 24;
            this.lDateSigning.Text = "Подписать до";
            // 
            // dtpDateOfAppointment
            // 
            this.dtpDateOfAppointment.CustomFormat = "dd.MM.yyyy H:m:s";
            this.dtpDateOfAppointment.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDateOfAppointment.Location = new System.Drawing.Point(133, 174);
            this.dtpDateOfAppointment.Name = "dtpDateOfAppointment";
            this.dtpDateOfAppointment.Size = new System.Drawing.Size(171, 22);
            this.dtpDateOfAppointment.TabIndex = 23;
            // 
            // lDateOfAppoiuntment
            // 
            this.lDateOfAppoiuntment.AutoSize = true;
            this.lDateOfAppoiuntment.Location = new System.Drawing.Point(2, 174);
            this.lDateOfAppoiuntment.Name = "lDateOfAppoiuntment";
            this.lDateOfAppoiuntment.Size = new System.Drawing.Size(125, 17);
            this.lDateOfAppoiuntment.TabIndex = 22;
            this.lDateOfAppoiuntment.Text = "Дата назначения";
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(133, 301);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(358, 22);
            this.textBox1.TabIndex = 21;
            // 
            // lDocPath
            // 
            this.lDocPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lDocPath.AutoSize = true;
            this.lDocPath.Location = new System.Drawing.Point(4, 301);
            this.lDocPath.Name = "lDocPath";
            this.lDocPath.Size = new System.Drawing.Size(123, 17);
            this.lDocPath.TabIndex = 20;
            this.lDocPath.Text = "Путь к документу";
            // 
            // btnGetRespons
            // 
            this.btnGetRespons.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGetRespons.Location = new System.Drawing.Point(474, 6);
            this.btnGetRespons.Name = "btnGetRespons";
            this.btnGetRespons.Size = new System.Drawing.Size(220, 34);
            this.btnGetRespons.TabIndex = 19;
            this.btnGetRespons.Text = "Получить ответственного";
            this.btnGetRespons.UseVisualStyleBackColor = true;
            this.btnGetRespons.Click += new System.EventHandler(this.btnGetRespons_Click);
            // 
            // tbMiddleName
            // 
            this.tbMiddleName.Enabled = false;
            this.tbMiddleName.Location = new System.Drawing.Point(133, 130);
            this.tbMiddleName.Name = "tbMiddleName";
            this.tbMiddleName.Size = new System.Drawing.Size(147, 22);
            this.tbMiddleName.TabIndex = 18;
            // 
            // tbFirstName
            // 
            this.tbFirstName.Enabled = false;
            this.tbFirstName.Location = new System.Drawing.Point(133, 102);
            this.tbFirstName.Name = "tbFirstName";
            this.tbFirstName.Size = new System.Drawing.Size(147, 22);
            this.tbFirstName.TabIndex = 17;
            // 
            // tbLastName
            // 
            this.tbLastName.Enabled = false;
            this.tbLastName.Location = new System.Drawing.Point(133, 74);
            this.tbLastName.Name = "tbLastName";
            this.tbLastName.Size = new System.Drawing.Size(147, 22);
            this.tbLastName.TabIndex = 16;
            // 
            // lName
            // 
            this.lName.AutoSize = true;
            this.lName.Location = new System.Drawing.Point(16, 71);
            this.lName.Name = "lName";
            this.lName.Size = new System.Drawing.Size(111, 17);
            this.lName.TabIndex = 15;
            this.lName.Text = "Ответственный";
            // 
            // FCreateDocument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(706, 335);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.lType);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dtpSigningDate);
            this.Controls.Add(this.lDateSigning);
            this.Controls.Add(this.dtpDateOfAppointment);
            this.Controls.Add(this.lDateOfAppoiuntment);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lDocPath);
            this.Controls.Add(this.btnGetRespons);
            this.Controls.Add(this.tbMiddleName);
            this.Controls.Add(this.tbFirstName);
            this.Controls.Add(this.tbLastName);
            this.Controls.Add(this.lName);
            this.Name = "FCreateDocument";
            this.ShowIcon = false;
            this.Text = "Зарегистрировать документ";
            this.Load += new System.EventHandler(this.FCreateDocument_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label lType;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker dtpSigningDate;
        private System.Windows.Forms.Label lDateSigning;
        private System.Windows.Forms.DateTimePicker dtpDateOfAppointment;
        private System.Windows.Forms.Label lDateOfAppoiuntment;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lDocPath;
        private System.Windows.Forms.Button btnGetRespons;
        private System.Windows.Forms.TextBox tbMiddleName;
        private System.Windows.Forms.TextBox tbFirstName;
        private System.Windows.Forms.TextBox tbLastName;
        private System.Windows.Forms.Label lName;
    }
}