﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolDirectory
{
    /// <summary>
    /// Окно присвоения документу статуса подписан
    /// </summary>
    public partial class FSignContract : Form
    {
        Guid docID;
        public FSignContract(Guid _docID)
        {
            InitializeComponent();
            docID = _docID;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Data.DoSignTheContract(docID, dtpDateOfAppointment.Value);
            this.Close();
        }
    }
}
