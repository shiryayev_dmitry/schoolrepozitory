﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolDirectory
{
    public partial class FDocumentation : Form
    {
        Worker worker = null;
        Timer timer = new Timer();
        public FDocumentation(Guid worker_id)
        {
            InitializeComponent();
            worker = Data.GetWorkerByDataId(worker_id);
            //Присвоить id пользователя, который был авторизован
            Helper.worker_id = worker.WorkerID;
            //Текущее время
            timer.Tick += new EventHandler(RefreshLabel);
            timer.Interval = 1000;
            timer.Start();
        }

        public void RefreshLabel(object sender, EventArgs e)
        {
            lDateTime.Text = DateTime.Now.ToString("G");
        }
        //При закрытии открыть окно авторизации
        private void FDocumentation_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form ifrm = Application.OpenForms[0];
            ifrm.Show();
        }

        private void FDocumentation_Load(object sender, EventArgs e)
        {
            //Проверить статус пользователя и открыть соответствующее окно для работы с доками
            if (worker.Positions.Status == Status.директор)
            {
                UCDirector ucDirector = new UCDirector();
                panel1.Controls.Add(ucDirector);
            }
            else
            {
                UCTeacher ucTeacher = new UCTeacher();
                panel1.Controls.Add(ucTeacher);
            }
        }

        private void btnOut_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
