﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolDirectory
{
    public partial class FWorkers : Form
    {
        public Worker worker;
        private List<Worker> workerList = new List<Worker>();

        public FWorkers()
        {
            InitializeComponent();
        }

        private void FWorkers_Load(object sender, EventArgs e)
        {
            int index = 0;
            foreach(Worker wr in Data.GetWorkerList())
            {
                dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells["lastName"].Value = wr.LastName;
                dataGridView1.Rows[index].Cells["firstName"].Value = wr.FirstName;
                dataGridView1.Rows[index].Cells["middleName"].Value = wr.MiddleName;
                dataGridView1.Rows[index].Cells["positions"].Value = wr.Positions.NamePosition.ToString();
                workerList.Add(wr);
                index++;
            }
            worker = workerList[0];
        }

        private void dataGridView1_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (dataGridView1.CurrentRow != null || e.RowIndex >= 0)
            {
                worker = workerList[e.RowIndex];
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            worker = null;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
