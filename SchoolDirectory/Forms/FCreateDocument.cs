﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolDirectory
{
    public partial class FCreateDocument : Form
    {
        Worker worker;
        Guid worker_id;
        public FCreateDocument()
        {
            InitializeComponent();
            
        }

        private void FCreateDocument_Load(object sender, EventArgs e)
        {
            //Задать начальное значение типа документа
            comboBox1.SelectedIndex = 0;
        }

        /// <summary>
        /// Получить и присвоить ответственного сотрудника
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGetRespons_Click(object sender, EventArgs e)
        {
            FWorkers fWorkers = new FWorkers();
            fWorkers.ShowDialog();
            worker = fWorkers.worker;
            if (worker != null)
            {
                tbLastName.Text = worker.LastName;
                tbFirstName.Text = worker.FirstName;
                tbMiddleName.Text = worker.MiddleName;
            }
        }
        /// <summary>
        /// Создать новый документ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            Documents doc = new Documents();
            doc.StartDate = dtpDateOfAppointment.Value;
            doc.EndDate = dtpSigningDate.Value;
            doc.WorkerID = Helper.worker_id;
            doc.ActingWorkerID = worker.WorkerID;
            doc.DocPath = textBox1.Text;
            doc.DocType = (DocType)comboBox1.SelectedIndex;
            Data.DoRegistryNewDocs(doc);

            this.Close();
        }
        /// <summary>
        /// Открыть окно выбора папки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog fbd = new OpenFileDialog();
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = fbd.FileName;
            }
        }
    }
}
