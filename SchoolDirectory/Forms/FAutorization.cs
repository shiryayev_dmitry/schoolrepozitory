﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolDirectory
{
    public partial class FAutorization : Form
    {
        public FAutorization()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Проверить логин пароль на совпадение
            Guid result = Data.CheckWorker(textBox1.Text, textBox2.Text);
            if (result != Guid.Empty)
            {
                //Открыть окно с документами
                FDocumentation fDocs = new FDocumentation(result);
                fDocs.Show();
                //Скрыть текущее окно (не закрыть)
                this.Hide();
            }
            else
            {
                MessageBox.Show("Не верный логин или пароль!");
            }
        }
    }
}
