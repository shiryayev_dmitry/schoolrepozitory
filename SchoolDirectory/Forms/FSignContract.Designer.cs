﻿namespace SchoolDirectory
{
    partial class FSignContract
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dtpDateOfAppointment = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Дата и время подписи";
            // 
            // dtpDateOfAppointment
            // 
            this.dtpDateOfAppointment.CustomFormat = "dd.MM.yyyy H:m:s";
            this.dtpDateOfAppointment.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDateOfAppointment.Location = new System.Drawing.Point(175, 9);
            this.dtpDateOfAppointment.Name = "dtpDateOfAppointment";
            this.dtpDateOfAppointment.Size = new System.Drawing.Size(171, 22);
            this.dtpDateOfAppointment.TabIndex = 24;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(249, 47);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 32);
            this.button1.TabIndex = 25;
            this.button1.Text = "Сохранить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FSignContract
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 81);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dtpDateOfAppointment);
            this.Controls.Add(this.label1);
            this.Name = "FSignContract";
            this.ShowIcon = false;
            this.Text = "Подписать договор";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpDateOfAppointment;
        private System.Windows.Forms.Button button1;
    }
}