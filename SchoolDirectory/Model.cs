﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolDirectory
{
 

    public class Worker
    {
        public Guid WorkerID { get; set; }
        public Positions Positions { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
    }

    public class WorkerData
    {
        public Guid WorkerDataID { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }

    public class Positions
    {
        public string NamePosition { get; set; }
        public Status Status { get; set; }
    }

    public enum Status
    {
        директор = 0,
        учитель = 1
    }

    public enum DocType
    {
        приказ = 0,
        распоряжение = 1
    }

    public class Documents: Worker
    {
        //public Guid WorkerID { get; set; }
        public Guid DocID { get; set; }
        public Guid ActingWorkerID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime RealizationDate { get; set; }
        public string DocPath { get; set; }
        public DocType DocType { get; set; }
    }

    public class DirectorDocuments
    {
        public Documents documents { get; set; }
        public Worker worker { get; set; }
    }
}
