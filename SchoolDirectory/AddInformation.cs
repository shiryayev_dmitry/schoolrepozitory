﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolDirectory
{
    /// <summary>
    /// Вспомогательные переменные
    /// </summary>
    public static class Helper
    {
        public static string PathToDb { get { return "../../School.mdb"; } }
        public static Guid worker_id { get; set; }
    }
}
