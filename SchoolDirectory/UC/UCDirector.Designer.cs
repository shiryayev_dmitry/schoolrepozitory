﻿namespace SchoolDirectory
{
    partial class UCDirector
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvDocuments = new System.Windows.Forms.DataGridView();
            this.Performer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CreateDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EndDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RealizeDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DocType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbSort = new System.Windows.Forms.ComboBox();
            this.lDocuments = new System.Windows.Forms.Label();
            this.btnCreateOrder = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocuments)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvDocuments
            // 
            this.dgvDocuments.AllowUserToAddRows = false;
            this.dgvDocuments.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvDocuments.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDocuments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDocuments.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Performer,
            this.CreateDate,
            this.EndDate,
            this.RealizeDate,
            this.DocType});
            this.dgvDocuments.Location = new System.Drawing.Point(7, 42);
            this.dgvDocuments.Name = "dgvDocuments";
            this.dgvDocuments.RowHeadersVisible = false;
            this.dgvDocuments.RowTemplate.Height = 24;
            this.dgvDocuments.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvDocuments.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDocuments.Size = new System.Drawing.Size(918, 355);
            this.dgvDocuments.TabIndex = 2;
            // 
            // Performer
            // 
            this.Performer.HeaderText = "Исполнитель";
            this.Performer.Name = "Performer";
            // 
            // CreateDate
            // 
            this.CreateDate.HeaderText = "Дата создания";
            this.CreateDate.Name = "CreateDate";
            // 
            // EndDate
            // 
            this.EndDate.HeaderText = "Конечная дата ";
            this.EndDate.Name = "EndDate";
            // 
            // RealizeDate
            // 
            this.RealizeDate.HeaderText = "Дата подписания";
            this.RealizeDate.Name = "RealizeDate";
            // 
            // DocType
            // 
            this.DocType.HeaderText = "Тип документа";
            this.DocType.Name = "DocType";
            // 
            // cbSort
            // 
            this.cbSort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSort.FormattingEnabled = true;
            this.cbSort.Items.AddRange(new object[] {
            "все",
            "приказ",
            "распоряжение"});
            this.cbSort.Location = new System.Drawing.Point(742, 8);
            this.cbSort.Name = "cbSort";
            this.cbSort.Size = new System.Drawing.Size(182, 24);
            this.cbSort.TabIndex = 0;
            this.cbSort.SelectedIndexChanged += new System.EventHandler(this.cbSort_SelectedIndexChanged);
            // 
            // lDocuments
            // 
            this.lDocuments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lDocuments.AutoSize = true;
            this.lDocuments.Location = new System.Drawing.Point(561, 11);
            this.lDocuments.Name = "lDocuments";
            this.lDocuments.Size = new System.Drawing.Size(175, 17);
            this.lDocuments.TabIndex = 4;
            this.lDocuments.Text = "Вывести документы типа";
            // 
            // btnCreateOrder
            // 
            this.btnCreateOrder.Location = new System.Drawing.Point(6, 3);
            this.btnCreateOrder.Name = "btnCreateOrder";
            this.btnCreateOrder.Size = new System.Drawing.Size(221, 33);
            this.btnCreateOrder.TabIndex = 5;
            this.btnCreateOrder.Text = "Зарегистрировать документ";
            this.btnCreateOrder.UseVisualStyleBackColor = true;
            this.btnCreateOrder.Click += new System.EventHandler(this.btnCreateOrder_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(233, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(171, 33);
            this.button1.TabIndex = 6;
            this.button1.Text = "Подписать";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(410, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(145, 33);
            this.button2.TabIndex = 7;
            this.button2.Text = "Открыть";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // UCDirector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnCreateOrder);
            this.Controls.Add(this.lDocuments);
            this.Controls.Add(this.cbSort);
            this.Controls.Add(this.dgvDocuments);
            this.Name = "UCDirector";
            this.Size = new System.Drawing.Size(927, 400);
            this.Load += new System.EventHandler(this.UCDirector_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocuments)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvDocuments;
        private System.Windows.Forms.ComboBox cbSort;
        private System.Windows.Forms.Label lDocuments;
        private System.Windows.Forms.Button btnCreateOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn Performer;
        private System.Windows.Forms.DataGridViewTextBoxColumn CreateDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn EndDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn RealizeDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn DocType;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}
