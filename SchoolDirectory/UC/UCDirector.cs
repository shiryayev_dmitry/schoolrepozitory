﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace SchoolDirectory
{
    /// <summary>
    /// Делопроизводство директора
    /// </summary>
    public partial class UCDirector : UserControl
    {
        public UCDirector()
        {
            InitializeComponent();
        }

        private List<DirectorDocuments> DirDocList = new List<DirectorDocuments>();
        /// <summary>
        /// Создать документ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCreateOrder_Click(object sender, EventArgs e)
        {
            FCreateDocument fCreate = new FCreateDocument();
            fCreate.ShowDialog();
            updateDataGrid();
        }
        /// <summary>
        /// Обновить список всех документов
        /// </summary>
        private void updateDataGrid()
        {
            int index = 0;
            cbSort.Items.Clear();
            dgvDocuments.Rows.Clear();
            DirDocList.Clear();

            cbSort.Items.Add("все");
            cbSort.Items.Add("приказ");
            cbSort.Items.Add("распоряжение");

            foreach (Documents doc in Data.GetDocList())
            {
                Worker worker = Data.GetWorkerByID(doc.ActingWorkerID);
                DirectorDocuments dirDoc = new DirectorDocuments();
                dirDoc.worker = worker;
                dirDoc.documents = doc;
                DirDocList.Add(dirDoc);
                dgvDocuments.Rows.Add();
                string performer = string.Format("{0} {1}.{2}.",
                    worker.LastName, worker.FirstName.Substring(0, 1), worker.MiddleName.Substring(0, 1));
                dgvDocuments.Rows[index].Cells["Performer"].Value = performer;
                dgvDocuments.Rows[index].Cells["CreateDate"].Value = doc.StartDate;
                dgvDocuments.Rows[index].Cells["EndDate"].Value = doc.EndDate;
                dgvDocuments.Rows[index].Cells["RealizeDate"].Value = doc.RealizationDate != new DateTime() ? doc.RealizationDate.ToShortDateString() : "";
                dgvDocuments.Rows[index].Cells["DocType"].Value = doc.DocType.ToString();
                if(!cbSort.Items.Contains(performer))
                    cbSort.Items.Add(performer);
                index++;
            }
        }
        /// <summary>
        /// Обновить списоке по определенному параметру
        /// </summary>
        /// <param name="typeDoc"></param>
        /// <param name="fio"></param>
        private void SetSortDataGridView(string typeDoc, string fio = null)
        {
            int index = 0;
            dgvDocuments.Rows.Clear();
            DirDocList.Clear();

            foreach (Documents doc in Data.GetDocList())
            {
                if(fio !=null)
                {
                    Worker worker = Data.GetWorkerByID(doc.ActingWorkerID);
                    string performer = string.Format("{0} {1}.{2}.",
                            worker.LastName, worker.FirstName.Substring(0, 1), worker.MiddleName.Substring(0, 1));
                    if (fio==performer)
                    {
                        
                        DirectorDocuments dirDoc = new DirectorDocuments();
                        dirDoc.worker = worker;
                        dirDoc.documents = doc;
                        DirDocList.Add(dirDoc);
                        dgvDocuments.Rows.Add();
                        
                        dgvDocuments.Rows[index].Cells["Performer"].Value = performer;
                        dgvDocuments.Rows[index].Cells["CreateDate"].Value = doc.StartDate;
                        dgvDocuments.Rows[index].Cells["EndDate"].Value = doc.EndDate;
                        dgvDocuments.Rows[index].Cells["RealizeDate"].Value = doc.RealizationDate != new DateTime() ? doc.RealizationDate.ToShortDateString() : "";
                        dgvDocuments.Rows[index].Cells["DocType"].Value = doc.DocType.ToString();
                        index++;
                    }
                }
                else if(doc.DocType.ToString().ToLower() == typeDoc.ToLower())
                {
                    Worker worker = Data.GetWorkerByID(doc.ActingWorkerID);
                    DirectorDocuments dirDoc = new DirectorDocuments();
                    dirDoc.worker = worker;
                    dirDoc.documents = doc;
                    DirDocList.Add(dirDoc);
                    dgvDocuments.Rows.Add();
                    string performer = string.Format("{0} {1}.{2}.",
                        worker.LastName, worker.FirstName.Substring(0, 1), worker.MiddleName.Substring(0, 1));
                    dgvDocuments.Rows[index].Cells["Performer"].Value = performer;
                    dgvDocuments.Rows[index].Cells["CreateDate"].Value = doc.StartDate;
                    dgvDocuments.Rows[index].Cells["EndDate"].Value = doc.EndDate;
                    dgvDocuments.Rows[index].Cells["RealizeDate"].Value = doc.RealizationDate != new DateTime() ? doc.RealizationDate.ToShortDateString() : "";
                    dgvDocuments.Rows[index].Cells["DocType"].Value = doc.DocType.ToString();
                    index++;
                }
            }
        }

        private void UCDirector_Load(object sender, EventArgs e)
        {
            updateDataGrid();
        }
        /// <summary>
        /// Открыть окно подписи
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if(dgvDocuments.CurrentRow != null )
            {
                FSignContract fSign = new FSignContract(DirDocList[dgvDocuments.CurrentRow.Index].documents.DocID);
                fSign.ShowDialog();
                updateDataGrid();
            }
        }
        /// <summary>
        /// Сортировка по ...
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbSort_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cbSort.SelectedIndex !=0 && cbSort.SelectedIndex < 3)
            {
                SetSortDataGridView(cbSort.Text);
            }
            else if(cbSort.SelectedIndex >2)
            {
                SetSortDataGridView("", cbSort.Text);
            }
            else
            {
                updateDataGrid();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dgvDocuments.CurrentRow != null)
            {
                Process.Start(DirDocList[dgvDocuments.CurrentRow.Index].documents.DocPath);
            }
        }
    }
}
