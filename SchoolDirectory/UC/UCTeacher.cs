﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace SchoolDirectory
{
    public partial class UCTeacher : UserControl
    {
        //Список документов
        private List<Documents> docList = new List<Documents>();

        public UCTeacher()
        {
            InitializeComponent();
        }

        private void updateGridView()
        {
            int index = 0;
            cbSort.Items.Clear();
            dataGridView1.Rows.Clear();
            docList.Clear();

            cbSort.Items.Add("все");
            cbSort.Items.Add("приказ");
            cbSort.Items.Add("распоряжение");
            foreach (Documents doc in Data.GetDocumentsByWorkerID(Helper.worker_id))
            {
                dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells["DateCreate"].Value = doc.StartDate;
                dataGridView1.Rows[index].Cells["EndDate"].Value = doc.EndDate;
                dataGridView1.Rows[index].Cells["DateSign"].Value = doc.RealizationDate != new DateTime() ? doc.RealizationDate.ToShortDateString() : "";
                dataGridView1.Rows[index].Cells["DocType"].Value = doc.DocType.ToString();
                docList.Add(doc);
                index++;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow != null)
            {
                FSignContract fSign = new FSignContract(docList[dataGridView1.CurrentRow.Index].DocID);
                fSign.ShowDialog();
                updateGridView();
            }
        }

        private void SetSortDataGridView(string typeDoc)
        {
            int index = 0;
            dataGridView1.Rows.Clear();
            docList.Clear();

            foreach (Documents doc in Data.GetDocumentsByWorkerID(Helper.worker_id))
            {
                if (doc.DocType.ToString().ToLower() == typeDoc.ToLower())
                {
                    dataGridView1.Rows.Add();
                    dataGridView1.Rows[index].Cells["DateCreate"].Value = doc.StartDate;
                    dataGridView1.Rows[index].Cells["EndDate"].Value = doc.EndDate;
                    dataGridView1.Rows[index].Cells["DateSign"].Value = doc.RealizationDate != new DateTime() ? doc.RealizationDate.ToShortDateString() : "";
                    dataGridView1.Rows[index].Cells["DocType"].Value = doc.DocType.ToString();
                    docList.Add(doc);
                    index++;
                }
            }
        }

        private void btnUpdateOrders_Click(object sender, EventArgs e)
        {
            updateGridView();
        }

        private void UCTeacher_Load(object sender, EventArgs e)
        {
            updateGridView();
        }

        private void cbSort_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbSort.SelectedIndex != 0 && cbSort.SelectedIndex < 3)
            {
                SetSortDataGridView(cbSort.Text);
            }
            else
            {
                updateGridView();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow != null)
            {
                Process.Start(docList[dataGridView1.CurrentRow.Index].DocPath);
            }
        }
    }
}
