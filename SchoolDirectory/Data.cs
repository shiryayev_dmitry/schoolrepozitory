﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolDirectory
{
    /// <summary>
    /// Работа с базой данных
    /// </summary>
    public static class Data
    {
        static Data()
        {
            DoConnection();
        }

        public static OleDbConnection cn;
        
        public static void DoConnection()
        {
            try
            {
                cn = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\"" +
                                 Helper.PathToDb + "\"; Jet OLEDB:Database Password=\"\";" +
                                 "Jet OLEDB:Engine Type=5;Jet OLEDB:Encrypt Database=True;");
                cn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Произошла ошибка в момент установления соединения с бд: " + ex.Message);
            }
        }

        public static Guid CheckWorker(string login, string password)
        {
            using (OleDbCommand cmd = cn.CreateCommand())
            {
                cmd.CommandText = @"SELECT workers_data_id FROM Workers_data WHERE worker_login=@worker_login AND
                                    worker_password=@worker_password";
                cmd.Parameters.AddWithValue("worker_login", login);
                cmd.Parameters.AddWithValue("worker_password", password);
                var result = cmd.ExecuteScalar();
                return result != null ? (Guid)result : Guid.Empty;
            }
        }

        public static Worker GetWorkerByID(Guid workerId)
        {
            Worker worker = new Worker();
            using (OleDbCommand cmd = cn.CreateCommand())
            {
                cmd.CommandText = @"SELECT ws.last_name, ws.first_name, 
                            ws.middle_name, ps.name_position, ps.status FROM (Workers AS ws 
                            INNER JOIN Positions AS ps ON ws.worker_position_id=ps.positions_id)
                            WHERE ws.worker_id=@worker_id";
                cmd.Parameters.AddWithValue("worker_id", workerId);
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    if(reader.Read())
                    {
                        int i = 0;
                        worker.WorkerID = workerId;
                        worker.LastName = reader.GetString(i++);
                        worker.FirstName = reader.GetString(i++);
                        worker.MiddleName = reader.GetString(i++);
                        worker.Positions = new Positions();
                        worker.Positions.NamePosition = reader.GetString(i++);
                        worker.Positions.Status = (Status)reader.GetInt32(i++);
                    }
                }
            }

            return worker;
        }

        public static Worker GetWorkerByDataId(Guid workerDataID)
        {
            Worker worker = new Worker();
            using (OleDbCommand cmd = cn.CreateCommand())
            {
                cmd.CommandText = @"SELECT ws.worker_id, ws.last_name, ws.first_name, 
                            ws.middle_name, ps.name_position, ps.status FROM (Workers AS ws 
                            INNER JOIN Positions AS ps ON ws.worker_position_id=ps.positions_id)
                            WHERE ws.workers_data_id=@workers_data_id";
                cmd.Parameters.AddWithValue("workers_data_id", workerDataID);

                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    if(reader.Read())
                    {
                        int i = 0;
                        worker.WorkerID = reader.GetGuid(i++);
                        worker.LastName = reader.GetString(i++);
                        worker.FirstName = reader.GetString(i++);
                        worker.MiddleName = reader.GetString(i++);
                        worker.Positions = new Positions();
                        worker.Positions.NamePosition = reader.GetString(i++);
                        worker.Positions.Status = (Status)reader.GetInt32(i++);
                    }
                }
            }
            return worker;
        }

        public static List<Worker> GetWorkerList()
        {
            List<Worker> workerList = new List<Worker>();
            using (OleDbCommand cmd = cn.CreateCommand())
            {
                cmd.CommandText = @"SELECT ws.worker_id, ws.last_name, ws.first_name, 
                            ws.middle_name, ps.name_position, ps.status FROM (Workers AS ws 
                            INNER JOIN Positions AS ps ON ws.worker_position_id=ps.positions_id)";
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while(reader.Read())
                    {
                        Worker worker = new Worker();
                        int i = 0;
                        worker.WorkerID = reader.GetGuid(i++);
                        worker.LastName = reader.GetString(i++);
                        worker.FirstName = reader.GetString(i++);
                        worker.MiddleName = reader.GetString(i++);
                        worker.Positions = new Positions();
                        worker.Positions.NamePosition = reader.GetString(i++);
                        worker.Positions.Status = (Status)reader.GetInt32(i++);
                        workerList.Add(worker);
                    }
                }
            }
            return workerList;
        }

        public static void DoRegistryNewDocs(Documents documents)
        {
            using (OleDbCommand cmd = cn.CreateCommand())
            {
                cmd.CommandText = @"INSERT INTO Documents (workers_id,doc_id,
                        acting_workers_id,start_date,end_date,doc_path, doc_type) VALUES (?,?,?,?,?,?,?)";
                cmd.Parameters.AddWithValue("workers_id", documents.WorkerID);
                cmd.Parameters.AddWithValue("doc_id", Guid.NewGuid());
                cmd.Parameters.AddWithValue("acting_workers_id", documents.ActingWorkerID);
                cmd.Parameters.AddWithValue("start_date", documents.StartDate.ToString("G"));
                cmd.Parameters.AddWithValue("end_date", documents.EndDate.ToString("G"));
                cmd.Parameters.AddWithValue("doc_path", documents.DocPath);
                cmd.Parameters.AddWithValue("doc_type", (Int32)documents.DocType);
                cmd.ExecuteNonQuery();
            }
        }

        public static List<Documents> GetDocumentsByWorkerID(Guid worker_id)
        {
            List<Documents> docList = new List<Documents>();
            using (OleDbCommand cmd = cn.CreateCommand())
            {
                cmd.CommandText = "SELECT * FROM Documents WHERE acting_workers_id=@acting_workers_id";
                cmd.Parameters.AddWithValue("acting_workers_id", worker_id);
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int i = 0;
                        Documents doc = new Documents();
                        doc.WorkerID = reader.GetGuid(i++);
                        doc.DocID = reader.GetGuid(i++);
                        doc.ActingWorkerID = reader.GetGuid(i++);
                        doc.StartDate = reader.GetDateTime(i++);
                        doc.EndDate = reader.GetDateTime(i++);
                        if (!reader.IsDBNull(i))
                        {
                            doc.RealizationDate = reader.GetDateTime(i++);
                        }
                        else
                        {
                            i++;
                        }

                        doc.DocPath = reader.GetString(i++);
                        doc.DocType = (DocType)reader.GetInt32(i++);

                        docList.Add(doc);
                    }
                }
            }
            return docList;
        }

        public static List<Documents> GetDocList()
        {
            List<Documents> docList = new List<Documents>();
            using (OleDbCommand cmd = cn.CreateCommand())
            {
                cmd.CommandText = "SELECT * FROM Documents";
                using (OleDbDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int i = 0;
                        Documents doc = new Documents();
                        doc.WorkerID = reader.GetGuid(i++);
                        doc.DocID = reader.GetGuid(i++);
                        doc.ActingWorkerID = reader.GetGuid(i++);
                        doc.StartDate = reader.GetDateTime(i++);
                        doc.EndDate = reader.GetDateTime(i++);
                        if(!reader.IsDBNull(i))
                        {
                            doc.RealizationDate = reader.GetDateTime(i++);
                        }
                        else
                        {
                            i++;
                        }

                        doc.DocPath = reader.GetString(i++);
                        doc.DocType = (DocType)reader.GetInt32(i++);

                        docList.Add(doc);
                    }
                }
            }
            return docList;
        }

        public static void DoSignTheContract(Guid DocID, DateTime realizeDate)
        {
            using (OleDbCommand cmd = cn.CreateCommand())
            {
                cmd.CommandText = "UPDATE Documents SET realization_date=@realization_date WHERE doc_id=@doc_id";
                cmd.Parameters.AddWithValue("realization_date", realizeDate.ToString("G"));
                cmd.Parameters.AddWithValue("doc_id", DocID);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
